import React, { Component } from "react";
import i18next from "i18next";
export default class Header extends Component {
  change(option) {
    localStorage.setItem("lang", option.target.value);
    window.location.reload();
  }
  render() {
    const lang = localStorage.getItem("lang") || "en";
    return (
      <nav className="container mb-4 mt-4">
        
        <h1>{i18next.t('Sample multi language')}</h1>

        <div className="row">
          <div className="col-10"></div>
          <div className="col-2">
            <select
              className="custom-select pull-right"
              onChange={this.change}
              value={lang}
            >
              <option value="en">english</option>
              <option value="vi">vietnam</option>
              <option value="jp">nihon</option>
            </select>
          </div>
        </div>
      </nav>
    );
  }
}
