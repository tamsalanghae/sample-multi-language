import React, { Component } from "react";
import axios from "axios";
import i18next from "i18next";

class Posts extends Component {
  state = {};
  componentDidMount() {
    axios.get("https://jsonplaceholder.typicode.com/posts").then((response) => {
      const posts = response.data;
      this.setState({ posts });
    });
  }
  render() {
    const posts = this.state.posts;
    if (posts)
      return posts.map((post) => {
        return (
          <div className="row mb-4">
            <div className="col-1"></div>
            <div className="col-8">
              <h2>{post.title}</h2>
              <p>{post.body}</p>
              <div className="row">
                <div className="col-8"></div>
                <div className="input-group col-4">
                    <input className="form-control blog-input" />
                    <div className="input-group-append">
                        <button className="btn btn-outline-secondary" type="button">{i18next.t('here')}</button>
                    </div>
                </div>
              </div>
            </div>
          </div>
        );
      });
    return "no post";
  }
}
export default Posts;
