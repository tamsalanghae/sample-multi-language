import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import common_en from './translations/en.json'
import common_vi from './translations/vi.json'
import common_jp from './translations/jp.json'

const resources = {
    en: {
        translation: common_en
    },
    vi: {
        translation: common_vi
    },
    jp: {
        translation: common_jp
    },
}
i18n
.use(initReactI18next)
.init({
    resources,
    lng: 'en',
    keySeparator: false,
    interpolation: {
        escapeValue: false
    }
})
export default i18n;